class Key {
  isDown: boolean;
  isPressed: boolean;

  constructor() {
    this.isDown = false;
    this.isPressed = false;
  }
}

interface KeysMap {
  [name: string]: string;
}

interface Keys {
  [code: string]: Key;
}

let keysMap: KeysMap = {};
let keys: Keys = {};

function init(map: KeysMap) {
  keysMap = map;
  keys = {};

  for (const name in keysMap) {
    keys[keysMap[name]] = new Key();
  }
}

function update() {
  for (const code in keys) {
    keys[code].isPressed = false;
  }
}

function isDown(name: string) {
  return keys[keysMap[name]].isDown;
}

function isPressed(name: string) {
  return keys[keysMap[name]].isPressed;
}

function onKeydown({ code }: KeyboardEvent) {
  const key = keys[code];

  if (!key || key.isDown) {
    return;
  }

  key.isDown = true;
  key.isPressed = true;
}

function onKeyup({ code }: KeyboardEvent) {
  const key = keys[code];

  if (!key) {
    return;
  }

  key.isDown = false;
  key.isPressed = false;
}

window.addEventListener('keydown', onKeydown);
window.addEventListener('keyup', onKeyup);

export default {
  init,
  update,
  isDown,
  isPressed,
};
